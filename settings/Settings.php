<?php

namespace settings;

class Settings
{
    use Singleton;

    private $someFields = [];

    private $routes = [
        'admin' => [
            'alias' => 'admin',
            'path' => 'core/admin/controller',
            '...' => ''
        ],
        'user' => [
            'path' => 'core/user/controller',
            '...' => ''
        ]
    ];

    static public function get($property) {
        return self::instance()->$property;
    }
}